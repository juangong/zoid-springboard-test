import zoid from 'zoid'
import { config } from './../config'

const element = 'zoid-component'
const options = {
  alternateText: 'Springboard',
  updateBackgroundColor: (elementId, color) => {
    const el = document.querySelector(`#${elementId}`)
    el.style.backgroundColor = color
  }
}

const helloWorld = zoid.create({
  tag: 'hello-world', // This has to be unique per js loaded on the page
  url: 'https://springboard.ddev.local:8444/',
  dimensions: {
    width: '600px',
    height: '2120px'
  },
  prerenderTemplate() {
    const html = document.createElement('html')
    html.innerHTML = '<body>Pre-render</body>'
    return html
  },
})

console.dir(helloWorld)
helloWorld.render(options, element)