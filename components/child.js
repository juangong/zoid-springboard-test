import zoid from 'zoid'
import { config } from './../config'

const helloWorld = zoid.create({
  tag: 'hello-world', // This has to be unique per js loaded on the page
  url: 'https://springboard.ddev.local:8444/'
})

document.addEventListener('click', () => {
  window.xprops.updateBackgroundColor('bgDiv', 'green')
}, false)

const element = document.querySelector('#hello')
element.innerHTML = `Hello ${window.xprops.alternateText}`

