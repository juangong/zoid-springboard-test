const path = require('path');

module.exports = env => {

  return ({
    entry: {
      parent: `./components/parent.js`,
      child: `./components/child.js`
    },
    output: {
      path: path.resolve(__dirname, `./dist`),
      filename: '[name].js'
    }
  })
};